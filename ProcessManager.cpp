/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "ProcessManager.h"
#include "ProcessEngineCommand.h"
#include "ShellResult.h"
#include "config.h"
#include "util.h"
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>
#include <process.hpp>
#include <string>
#include <utility>

using json = nlohmann::json;


ProcessManager::ProcessManager(std::string basePath,
                               std::string workflowDirectory,
                               std::string engineFile, std::string statusFile)
    : basePath(std::move(basePath)),
      workflowsDirectory(std::move(workflowDirectory)),
      engineFile(std::move(engineFile)), workflowFile(std::move(statusFile)),
      nextId(0) {

  if (this->engineFile.empty()) {
    this->engineFile = config::DEFAULT_ENGINE_FILENAME;
  }
  if (this->workflowFile.empty()) {
    this->workflowFile = config::DEFAULT_WORKFLOW_FILENAME;
  }

  loadAvailableEngineConfigurations();
  restore();
}

json ProcessManager::startWorkflow(const std::string &workflowFile,
                                   const std::string &workflowEngine,
                                   bool noColors) {
  std::string workflowFileAbsolute = workflowFile;
  if (!util::isAbsolutePath(workflowFile)) {
    workflowFileAbsolute = util::getAbsolutePath(workflowFile);
  }
  if (!util::fileExists(workflowFileAbsolute)) {
    throw std::runtime_error("Can not open workflow file: " +
                             workflowFileAbsolute);
  }
  std::string engineName =
    workflowEngine.empty() ? defaultProcessEngine : workflowEngine;
  const auto engine = getProcessEngine(engineName, true);

  std::string workflowFolder = getWorkflowExecutionFolder(nextId);

  if (!util::createDirectory(workflowFolder)) {
    bool retry_success = false;
    for (int i = 1; i < config::MAX_CREATE_FOLDER_RETRIES; i++) {
      nextId++; // nextId can not be used because the folder could not be created
      workflowFolder = getWorkflowExecutionFolder(nextId);
      if (util::createDirectory(workflowFolder)) {
        retry_success = true;
        break;
      }
    }
    if (!retry_success) {
      throw std::runtime_error(
        "Can not create directory for workflow execution: " + workflowFolder);
    }
  }
  save(nextId + 1);

  ProcessEngineCommand engineCommand(engine->getPath(), "start");
  if (noColors) { engineCommand.addArgument("--no-color"); }
  engineCommand.addArgument("-p");
  engineCommand.addArgument(workflowFolder);
  engineCommand.addArgument(workflowFileAbsolute);
  engineCommand.executeNonBlocking();

  // instead of waiting for a result, we request the status directly afterwards
  json returnedJson;
  try {
    json engineJsonResult;
    int attempt = 0;
    while (engineJsonResult.empty() &&
           attempt < config::REQUEST_STATUS_AFTER_START_ATTEMPTS) {
      engineJsonResult = retrieveWorkflowStatus(workflowFolder, engineName);
      std::this_thread::sleep_for(config::REQUEST_STATUS_AFTER_START_DELAY);
      attempt++;
    }

    auto createdWorkflow = std::make_unique<Workflow>();
    createdWorkflow->fromJson(engineJsonResult);
    createdWorkflow->setId(nextId);

    returnedJson = createdWorkflow->getJson();
    std::cout << std::setw(4) << returnedJson << std::endl;
    workflows[createdWorkflow->getId()] = std::move(createdWorkflow);
  } catch (const std::exception &exception) {
    throw std::runtime_error(
      "Could not retrieve execution status from process engine. " +
      std::string(exception.what()));
  }
  save(++nextId);

  return returnedJson;
}

bool ProcessManager::updateWorkflow(Workflow *workflow,
                                    bool forceRefresh) const {
  bool workflowUpdated = false;
  WorkflowState lastKnownState = workflow->getState();
  if (forceRefresh || lastKnownState == RUNNING || lastKnownState == READY ||
      lastKnownState == NEEDS_INTERACTION || lastKnownState == CANCELLING) {
    try {
      json jsonStatus = retrieveWorkflowStatus(workflow->getId());
      if (!jsonStatus.empty()) {
        workflowUpdated = true;
        if (jsonStatus.contains("id")) {
          // do not let process engines overwrite the id
          jsonStatus.erase("id");
        }
        workflow->fromJson(jsonStatus);
      }
    } catch (const std::exception &exception) {
      error(exception.what());
      return false;
    }
  }
  return workflowUpdated;
}

void ProcessManager::loadAvailableEngineConfigurations() {
  std::string configFilePath = basePath + engineFile;

  // fall back to the default engine configuration, if possible (.deb installation copies a default config to a system path)
  if (!util::fileExists(configFilePath) &&
      util::fileExists(config::DEFAULT_ENGINE_CONFIGURATION_PATH)) {
    configFilePath = config::DEFAULT_ENGINE_CONFIGURATION_PATH;
  }

  json jsonObj;
  std::ifstream in(configFilePath);
  if (util::fileExists(configFilePath)) {
    try {
      in >> jsonObj;
    } catch (nlohmann::detail::parse_error &error) {
      std::cerr << "Error: Unable to read process engine descriptions from "
                << configFilePath << std::endl;
      std::cerr << error.what() << std::endl; // debug
      return;
    }
  }

  json engineArray = jsonObj[config::JSON_ENGINES_KEY];
  if (!engineArray.empty() && engineArray.type() == json::value_t::array) {
    for (auto &it : engineArray) {
      auto engine = std::make_unique<ProcessEngine>();
      engine->fromJson(it);
      processEngines[engine->getName()] = std::move(engine);
    }
  } else {
    std::cerr << "Warning: No process engine descriptions found in "
              << configFilePath << std::endl;
  }
  if (jsonObj.contains(config::JSON_DEFAULT_ENGINE_KEY) &&
      !jsonObj[config::JSON_DEFAULT_ENGINE_KEY].empty()) {
    defaultProcessEngine = jsonObj[config::JSON_DEFAULT_ENGINE_KEY];
  } else if (!processEngines.empty()) {
    std::cerr << "Warning: No default process engine specified in "
              << configFilePath << std::endl;
  }
}

void ProcessManager::restore() {
  std::string fileName = basePath + workflowFile;

  std::ifstream in(fileName);
  json jsonObj;
  if (util::fileExists(fileName)) {
    try {
      in >> jsonObj;
    } catch (nlohmann::detail::parse_error &error) {
      std::cout << "No workflow descriptions to restore." << std::endl;
    }
  }

  if (jsonObj.contains(config::JSON_NEXTID_KEY) &&
      jsonObj[config::JSON_NEXTID_KEY].is_number()) {
    nextId = jsonObj[config::JSON_NEXTID_KEY].get<int>();
  } else {
    nextId = 0;
    if (!workflows.empty()) {
      throw std::logic_error(
        "Next workflow id could not be found in non-empty persistence file");
    }
  }
  json workflowArray = jsonObj["workflows"];
  if (!workflowArray.empty() && workflowArray.type() == json::value_t::array) {
    for (auto &it : workflowArray) {
      if (it != nullptr) {
        auto workflow = std::make_unique<Workflow>();
        workflow->fromJson(it);
        workflows[workflow->getId()] = std::move(workflow);
      }
    }
  }
}

void ProcessManager::save(int nextFreeId) const {
  std::string fileName = basePath + workflowFile;
  std::ofstream out(fileName);
  json newObj;
  for (const auto &workflow : workflows) {
    newObj[config::JSON_WORKFLOWS_KEY].push_back(workflow.second->getJson());
  }
  newObj[config::JSON_NEXTID_KEY] = nextFreeId;
  out << std::setw(4) << newObj << std::endl;
}

void ProcessManager::save() const { save(nextId); }

json ProcessManager::retrieveWorkflowStatus(
  const std::string &workflowFolder, const std::string &engineName) const {
  const ProcessEngine *engine = getProcessEngine(engineName, false);
  if (engine != nullptr) {
    ProcessEngineCommand engineCommand(engine->getPath(), "status");
    engineCommand.addArgument("-p");
    engineCommand.addArgument(workflowFolder);
    ShellResult result = engineCommand.execute();
    if (result.exit_code != 0) {
      std::ostringstream logStream;
      logStream
        << "Process Engine returned non zero exit code when retrieving status: "
        << result.stderr_raw;
      log(logStream.str());
      throw std::runtime_error(logStream.str());
    }
    return result.parseStdoutAsJson();
  } else {
    throw std::logic_error("Process Engine not found");
  }
}

json ProcessManager::retrieveWorkflowStatus(unsigned int id) const {
  auto workflow = getExistingWorkflow(id, false);
  if (workflow == nullptr) { throw std::logic_error("Unknown workflow"); }
  return retrieveWorkflowStatus(getWorkflowExecutionFolder(id),
                                workflow->getProcessEngine());
}

std::string ProcessManager::getWorkflowExecutionFolder(unsigned int id) const {
  return basePath + workflowsDirectory + std::to_string(id) + "/";
}

void ProcessManager::log(const std::string &message) const {
  if (config::LOGGING_ENABLED) {
    std::string logFileName = basePath + config::LOGFILE;
    std::ofstream logStream;

    logStream.open(logFileName, std::ios_base::app); // append mode

    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    logStream << "[" << std::put_time(&tm, config::DATETIME_FORMAT.c_str())
              << "] " << message << std::endl;
  }
}

const ProcessEngine *
ProcessManager::getProcessEngine(const std::string &engineName,
                                 bool logErrors) const {
  auto engineItem = processEngines.find(engineName);
  if (engineItem != processEngines.end()) {
    return engineItem->second.get();
  } else {
    std::ostringstream logStream;
    logStream << "Process engine with the name " << engineName << " not found.";
    if (logErrors) { log(logStream.str()); }
    throw std::runtime_error(logStream.str());
  }
}

Workflow *ProcessManager::getExistingWorkflow(unsigned int workflowId,
                                              bool logErrors) const {
  auto workflowItem = workflows.find(workflowId);
  if (workflowItem != workflows.end()) {
    return workflowItem->second.get();
  } else {
    std::ostringstream logStream;
    logStream << "Workflow with id " << std::to_string(workflowId)
              << " not found.";
    if (logErrors) { log(logStream.str()); }
    throw std::runtime_error(logStream.str());
  }
}

void ProcessManager::continueWorkflow(unsigned int workflowId) {
  auto workflow = getExistingWorkflow(workflowId, true);
  auto engine = getProcessEngine(workflow->getProcessEngine(), true);

  std::string workflowFolder = getWorkflowExecutionFolder(workflowId);
  ProcessEngineCommand engineCommand(engine->getPath(), "continue");
  engineCommand.addArgument("-p");
  engineCommand.addArgument(workflowFolder);
  ShellResult result = engineCommand.execute();
  if (result.exit_code != 0) {
    std::ostringstream logStream;
    logStream << "Process Engine returned non zero exit code when attempting "
                 "to continue workflow execution: "
              << result.stderr_raw;
    throw std::runtime_error(logStream.str());
  }
}

void ProcessManager::cancelWorkflow(unsigned int workflowId) {
  auto workflow = getExistingWorkflow(workflowId, true);
  auto engine = getProcessEngine(workflow->getProcessEngine(), true);

  std::string workflowFolder = getWorkflowExecutionFolder(workflowId);
  ProcessEngineCommand engineCommand(engine->getPath(), "cancel");
  engineCommand.addArgument("-p");
  engineCommand.addArgument(workflowFolder);
  ShellResult result = engineCommand.execute(true);
  if (result.exit_code != 0) {
    std::ostringstream logStream;
    logStream << "Process Engine returned non zero exit code when attempting "
                 "to cancel workflow execution: "
              << result.stderr_raw;
    throw std::runtime_error(logStream.str());
  }
  std::cout << result.stdout_raw << std::endl;
}

void ProcessManager::error(const std::string &message) const {
  std::cerr << message << std::endl;
  log("Error: " + message);
}

void ProcessManager::inputInteractionValue(unsigned int workflowId,
                                           std::string interactionId,
                                           const std::string &inputValue) {
  auto workflow = getExistingWorkflow(workflowId, true);
  auto engine = getProcessEngine(workflow->getProcessEngine(), true);

  std::string workflowFolder = getWorkflowExecutionFolder(workflowId);
  ProcessEngineCommand engineCommand(engine->getPath(), "input");
  engineCommand.addArgument("-p");
  engineCommand.addArgument(workflowFolder);
  engineCommand.addArgument(std::move(interactionId));
  engineCommand.addArgument(inputValue);
  ShellResult result = engineCommand.execute();
  if (result.exit_code != 0) {
    std::ostringstream logStream;
    logStream << "Process Engine returned non zero exit code when attempting "
                 "to input interaction value: "
              << result.stderr_raw;
    throw std::runtime_error(logStream.str());
  }
}

json ProcessManager::getWorkflowList() {
  json workflowsArray = json::array();
  bool workflowUpdated = false;
  for (const auto &workflow : workflows) {
    // refresh workflow info if workflow is not finished
    workflowUpdated |= updateWorkflow(workflow.second.get());

    workflowsArray.push_back(workflow.second->getJson());
  }

  if (workflowUpdated) { save(); }

  return workflowsArray;
}

json ProcessManager::getEnginesList() const {
  json result = json::array();
  for (const auto &engine : processEngines) {
    result.push_back(engine.second->getJson());
  }
  return result;
}

std::string ProcessManager::getLog(unsigned int workflowId) {

  auto workflow = getExistingWorkflow(workflowId, false);
  auto engine = getProcessEngine(workflow->getProcessEngine(), false);

  std::string workflowFolder = getWorkflowExecutionFolder(workflowId);
  ProcessEngineCommand engineCommand(engine->getPath(), "log");
  engineCommand.addArgument("-p");
  engineCommand.addArgument(workflowFolder);
  ShellResult result = engineCommand.execute();
  if (result.exit_code != 0) {
    std::ostringstream logStream;
    logStream << "Process Engine returned non zero exit code when retrieving "
                 "the workflow execution log: "
              << result.stderr_raw;
    throw std::runtime_error(logStream.str());
  }
  return result.stdout_raw;
}

std::string ProcessManager::getLogPath(unsigned int workflowId) const {
  auto workflow = getExistingWorkflow(workflowId, false);
  auto engine = getProcessEngine(workflow->getProcessEngine(), false);

  std::string workflowFolder = getWorkflowExecutionFolder(workflowId);
  ProcessEngineCommand engineCommand(engine->getPath(), "log_path");
  engineCommand.addArgument("-p");
  engineCommand.addArgument(workflowFolder);
  ShellResult result = engineCommand.execute();
  if (result.exit_code != 0) {
    std::ostringstream logStream;
    logStream << "Process Engine returned non zero exit code when retrieving "
                 "the path to the log file: "
              << result.stderr_raw;
    throw std::runtime_error(logStream.str());
  }
  return result.stdout_raw;
}

json ProcessManager::getShortcuts(unsigned int workflowId) {
  auto workflow = getExistingWorkflow(workflowId, true);
  auto engine = getProcessEngine(workflow->getProcessEngine(), true);

  std::string workflowFolder = getWorkflowExecutionFolder(workflowId);
  ProcessEngineCommand engineCommand(engine->getPath(), "shortcuts");
  engineCommand.addArgument("-p");
  engineCommand.addArgument(workflowFolder);
  ShellResult result = engineCommand.execute();
  if (result.exit_code != 0) {
    std::ostringstream logStream;
    logStream << "Process Engine returned non zero exit code when retrieving "
                 "the workflow shortcuts: "
              << result.stderr_raw;
    throw std::runtime_error(logStream.str());
  }
  return result.parseStdoutAsJson();
}

json ProcessManager::getInteractions(unsigned int workflowId) {
  auto workflow = getExistingWorkflow(workflowId, true);
  auto engine = getProcessEngine(workflow->getProcessEngine(), true);

  std::string workflowFolder = getWorkflowExecutionFolder(workflowId);
  ProcessEngineCommand engineCommand(engine->getPath(), "interactions");
  engineCommand.addArgument("-p");
  engineCommand.addArgument(workflowFolder);
  ShellResult result = engineCommand.execute();
  if (result.exit_code != 0) {
    std::ostringstream logStream;
    logStream << "Process Engine returned non zero exit code when retrieving "
                 "the workflow interactions: "
              << result.stderr_raw;
    throw std::runtime_error(logStream.str());
  }

  return result.parseStdoutAsJson();
}

json ProcessManager::getWorkflowStatus(unsigned int workflowId,
                                       bool forceRefresh) {
  auto workflow = getExistingWorkflow(workflowId, true);
  if (workflow) {
    if (updateWorkflow(workflow, forceRefresh)) { save(); }
  }
  return workflow->getJson();
}

bool ProcessManager::hasWorkflow(unsigned int workflowId) {
  return workflows.find(workflowId) != workflows.end();
}

const std::string &ProcessManager::getBasePath() const { return basePath; }
