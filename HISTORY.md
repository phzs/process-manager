# Release history

## 0.5.0 (unreleased)
* Added subcommand log_path to retrieve the path to the log file for a single workflow execution

## 0.4.0 (2023-03-29)

* Added flag to force refresh for status command
* Save updates from status command as well
* Fixed bug with status update when cancelling a workflow

## 0.3.2 (2023-02-21)

* Updated CI config
* Updated debian packaging

## 0.3.1 (2023-01-25)

* Removed additional quotes when passing input values to the process engine, which led to problems with values in json format

## 0.3.0 (2022-11-22)

* Added cancel functionality including a timeout option
* Reduced necessary engine configuration per engine to "name" and "path" in engines.json
  * Users need to add "path" in their $HOME/.process_manager/engines.json to avoid a warning about the default path
    being used (and may also remove the other config entries)
* Improved fallback mechanism for the config
* Always detach the process when starting a new workflow via a process engine
* Removed timeout functionality (and respective CLI option) for cancelling workflows, since this is now the responsibility of the Process Engine
* Updates dependencies
* Improved CI

## 0.2.1 (2022-09-21)

* Updated default engine configuration for Process Engine v0.6.0 (the old configuration is still working)
  * It is recommended to update the configuration (engines.json), see res/engines.json for the updated default
    configuration

## 0.2.0 (2021-09-16)

* Added code refactorings
* Added REST service
* Implemented timeout mechanism to avoid blocking in case the process engine takes longer as expected to respond

## 0.1.3 (2021-07-21)

* Added bugfixes
* Updates dependencies

## 0.1.2 (2021-05-11)

* Added bugfixes

## 0.1.1 (2021-05-07)

* Added CI configuration
* Normalized code style using clang-format for readability
* Added smaller bugfixes

## 0.1.0 (2021-01-15)

* Basic process manager functionality implemented.
