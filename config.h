/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <chrono>
#include <string>

// constants for configuration
// might be read from a configuration file in future
namespace config {
  using namespace std::chrono_literals;

  const bool LOGGING_ENABLED = false;
  const std::string LOGFILE = "process_manager.log";
  const std::string DATETIME_FORMAT = "%d.%m.%Y %H:%M:%S";

  // parameterizing process engine commands
  const std::string PAR_REPLACE_TOKEN = "%s";

  // JSON keys
  const std::string JSON_NEXTID_KEY = "next_id";
  const std::string JSON_WORKFLOWS_KEY = "workflows";
  const std::string JSON_ENGINES_KEY = "engines";
  const std::string JSON_DEFAULT_ENGINE_KEY = "default_engine";

  // default values
  const std::string DEFAULT_ENGINE_FILENAME = "engines.json";
  const std::string DEFAULT_WORKFLOW_FILENAME = "workflows.json";
  const std::string DEFAULT_ENGINE_CONFIGURATION_PATH =
    "/etc/process_manager/engines.json";

  // retries and waits
  const int MAX_CREATE_FOLDER_RETRIES = 5;
  const int ENGINE_TIMEOUT_SECONDS = 2;
  const int REQUEST_STATUS_AFTER_START_ATTEMPTS = 100;
  const auto REQUEST_STATUS_AFTER_START_DELAY = 50ms;

  // accepting workflows from the network
  const std::string REST_WORKFLOW_FOLDER_NAME = "web";

  const std::string PROCESS_ENGINE_DEFAULT_PATH = "process_engine";
} // namespace config
