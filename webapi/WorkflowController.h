/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../net/ApiController.h"

class ProcessManager;

namespace webapi {

  /**
   * @brief Controller for all workflow endpoints.
   * 
   */
  class WorkflowController : public net::ApiController {
  public:
    /**
     * @brief Construct a new WorkflowController object.
     * 
     * @param manager The ProcessManager.
     * @param server The server.
     * @param basePath The path prefix for all endpoints of this controller.
     */
    WorkflowController(ProcessManager *manager, httplib::Server *server,
                       const std::string &basePath);
    /**
     * @brief Destroy the WorkflowController object.
     * 
     */
    ~WorkflowController() = default;

  private:
    void addWorkflowsEndpoint();
    void addWorkflowEndpoint();
    void addStartWorkflowEndpoint();
    void addContinueWorkflow();
    void addLogEndpoint();
    void addShortcutsEndpoint();
    void addInteractionsEndpoint();
    void addInputEndpoint();

  private:
    void getWorkflows(const httplib::Request &request,
                      httplib::Response &response);

    void getWorkflow(const httplib::Request &request,
                     httplib::Response &response);

    void getWorkflowLog(const httplib::Request &request,
                        httplib::Response &response);

    void getWorkflowShortcuts(const httplib::Request &request,
                              httplib::Response &response);

    void getWorkflowInteractions(const httplib::Request &request,
                                 httplib::Response &response);

    void addInput(const httplib::Request &request, httplib::Response &response,
                  const httplib::ContentReader &contentReader);

    void startWorkflow(const httplib::Request &request,
                       httplib::Response &response,
                       const httplib::ContentReader &contentReader);

    void continueWorkflow(const httplib::Request &request,
                          httplib::Response &response);

    bool saveUploadedWorkflow(const std::string &workflowContent,
                              std::string &absoluteFilePath);

  private:
    ProcessManager *manager;
    static constexpr int workflowIdRouteValueIndex{1};
    const std::string workflowIdRouteValue;
  };

} // namespace webapi