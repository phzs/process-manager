/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "WorkflowController.h"
#include "../ProcessManager.h"
#include "../config.h"
#include "../net/MimeTypes.h"
#include "../net/ModelError.h"
#include "../net/RouteParam.h"
#include "../net/Statuscodes.h"
#include "../util.h"
#include "Schemas.h"

namespace webapi {
  WorkflowController::WorkflowController(ProcessManager *manager,
                                         httplib::Server *server,
                                         const std::string &basePath)
      : manager(manager),
        workflowIdRouteValue(net::routeparam::create("workflowId", "integer")),
        net::ApiController(server, net::UriPath(basePath) / "workflows") {

    setName("workflow");

    addWorkflowsEndpoint();
    addWorkflowEndpoint();
    addStartWorkflowEndpoint();
    addContinueWorkflow();
    addLogEndpoint();
    addShortcutsEndpoint();
    addInteractionsEndpoint();
    addInputEndpoint();
  }

  void WorkflowController::addWorkflowsEndpoint() {
    // GET /workflows
    auto &info =
      addGetEndpoint(net::UriPath(), this, &WorkflowController::getWorkflows);

    info.summary = "Finds all workflows";
    info.description = "Returns all workflows regardless of their state.";

    info.responses[200] = {"Workflows",
                           {{"application/json", schemas::WORKFLOWS}}};
  }

  void WorkflowController::addWorkflowEndpoint() {
    // GET /workflows/{workflowId:int}
    auto &info = addGetEndpoint(net::UriPath() / workflowIdRouteValue, this,
                                &WorkflowController::getWorkflow);

    info.summary = "Finds a specific workflow.";
    info.description = "Returns a workflow specified by an id.";

    info.responses[net::statuscodes::OK] = {
      "The specified workflow was found.",
      {{net::mimetypes::APPLICATION_JSON,
        net::openapi::refToSchema("Workflow")}}};

    info.responses[net::statuscodes::NOT_FOUND] = {
      "The specified workflow was not found."};
  }

  void WorkflowController::addStartWorkflowEndpoint() {
    // POST /workflows
    auto &info =
      addPostEndpoint(net::UriPath(), this, &WorkflowController::startWorkflow);

    info.summary = "Starts a workflow";
    info.description =
      "Starts a workflow with the specified engine and workflow file.";

    info.requestBody.description = "Starts a workflow";
    info.requestBody.required = true;

    info.requestBody.content[net::mimetypes::APPLICATION_JSON] =
      schemas::STARTWORKFLOW_APPLICATION_JSON;

    info.requestBody.content[net::mimetypes::MULTIPART_FORMDATA] =
      schemas::STARTWORKFLOW_MULTIPART_FORMDATA;

    info.responses[net::statuscodes::CREATED] = {
      "The started workflow.",
      {{net::mimetypes::APPLICATION_JSON,
        net::openapi::refToSchema("Workflow")}}};

    info.responses[net::statuscodes::BAD_REQUEST] = {
      "Request was malformed.",
      {{net::mimetypes::APPLICATION_JSON,
        net::openapi::refToSchema("ModelError")}}};
  }

  void WorkflowController::addContinueWorkflow() {

    // PUT /workflows/{workflowId:int}/state
    auto &info = addPutEndpoint(net::UriPath() / workflowIdRouteValue / "state",
                                this, &WorkflowController::continueWorkflow);

    info.summary = "Continues a workflow.";
    info.description = "Continues a workflow if no interactions are needed.";

    info.responses[net::statuscodes::OK] = {
      "The current state of the workflow.",
      {{net::mimetypes::APPLICATION_JSON, schemas::WORKFLOW_STATE}}};
  }

  void WorkflowController::addLogEndpoint() {
    // Get /workflows/{workflowId:int}/log
    auto &info = addGetEndpoint(net::UriPath() / workflowIdRouteValue / "log",
                                this, &WorkflowController::getWorkflowLog);

    info.summary = "Returns the log of a workflow";
    info.description = "Returns the log of a workflow";

    info.responses[net::statuscodes::OK] = {
      "The log.",
      {{net::mimetypes::TEXT_PLAIN, R"({"type": "string"})"_json}}};


    info.responses[net::statuscodes::NOT_FOUND] = {
      "The specified workflow was not found."};
  }

  void WorkflowController::addShortcutsEndpoint() {
    // Get /workflows/{workflowId:int}/shortcuts
    auto &info =
      addGetEndpoint(net::UriPath() / workflowIdRouteValue / "shortcuts", this,
                     &WorkflowController::getWorkflowShortcuts);

    info.summary = "Returns all shortcuts of a workflow";
    // TODO: Response.

    info.responses[net::statuscodes::NOT_FOUND] = {
      "The specified workflow was not found."};
  }

  void WorkflowController::addInteractionsEndpoint() {
    // Get /workflows/{workflowId:int}/interactions
    auto &info =
      addGetEndpoint(net::UriPath() / workflowIdRouteValue / "interactions",
                     this, &WorkflowController::getWorkflowInteractions);

    info.summary = "Returns all interactions of a workflow.";
    info.description = "Returns all interactions of a workflow that are "
                       "reached until the current node.";

    info.responses[net::statuscodes::OK] = {
      "The specified workflow was not found.",
      {{net::mimetypes::APPLICATION_JSON, schemas::INTERACTIONS}}};

    info.responses[net::statuscodes::NOT_FOUND] = {
      "The specified workflow was not found."};
  }

  void WorkflowController::addInputEndpoint() {
    // Patch /workflows/{workflowId:int}/input
    auto &info = addPatchEndpointWithReader(
      net::UriPath() / workflowIdRouteValue / "input", this,
      &WorkflowController::addInput);

    info.summary = "Changes the input for an interaction.";
    info.description =
      "All interactions reached to the current node can be changed.";

    info.requestBody.required = true;

    info.requestBody.content[net::mimetypes::APPLICATION_JSON] =
      schemas::INPUTS;

    info.responses[net::statuscodes::OK] = {"Inputs are set."};

    info.responses[net::statuscodes::BAD_REQUEST] = {
      "The request was malformed.",
      {{net::mimetypes::APPLICATION_JSON,
        net::openapi::refToSchema("ModelError")}}};

    info.responses[net::statuscodes::NOT_FOUND] = {
      "The specified workflow was not found."};
  }

  void WorkflowController::getWorkflows(const httplib::Request &request,
                                        httplib::Response &response) {
    response.set_content(to_string(manager->getWorkflowList()),
                         net::mimetypes::APPLICATION_JSON);
  }

  void WorkflowController::getWorkflow(const httplib::Request &request,
                                       httplib::Response &response) {
    auto workflowId = getIntRouteValue(request, workflowIdRouteValueIndex);

    if (!manager->hasWorkflow(workflowId)) {
      response.status = net::statuscodes::NOT_FOUND;
      return;
    }

    response.set_content(to_string(manager->getWorkflowStatus(workflowId)),
                         net::mimetypes::APPLICATION_JSON);
  }

  void WorkflowController::getWorkflowLog(const httplib::Request &request,
                                          httplib::Response &response) {
    auto workflowId = getIntRouteValue(request, workflowIdRouteValueIndex);

    if (!manager->hasWorkflow(workflowId)) {
      response.status = net::statuscodes::NOT_FOUND;
      return;
    }

    response.set_content(manager->getLog(workflowId),
                         net::mimetypes::TEXT_PLAIN);
  }

  void WorkflowController::getWorkflowShortcuts(const httplib::Request &request,
                                                httplib::Response &response) {
    auto workflowId = getIntRouteValue(request, workflowIdRouteValueIndex);

    if (!manager->hasWorkflow(workflowId)) {
      response.status = net::statuscodes::NOT_FOUND;
      return;
    }

    response.set_content(to_string(manager->getShortcuts(workflowId)),
                         net::mimetypes::APPLICATION_JSON);
  }

  void
  WorkflowController::getWorkflowInteractions(const httplib::Request &request,
                                              httplib::Response &response) {

    auto workflowId = getIntRouteValue(request, workflowIdRouteValueIndex);

    if (!manager->hasWorkflow(workflowId)) {
      response.status = net::statuscodes::NOT_FOUND;
      return;
    }

    response.set_content(to_string(manager->getInteractions(workflowId)),
                         net::mimetypes::APPLICATION_JSON);
  }

  void
  WorkflowController::addInput(const httplib::Request &request,
                               httplib::Response &response,
                               const httplib::ContentReader &contentReader) {

    // The endpoint only supports application/json.
    if (request.get_header_value("Content-Type") !=
        net::mimetypes::APPLICATION_JSON) {
      response.status = net::statuscodes::UNSUPPORTED_MEDIA_TYPE;
      return;
    }

    // Get the id of the workflow.
    auto workflowId = getIntRouteValue(request, workflowIdRouteValueIndex);

    // Always read the content.
    // Unread content leads to exceptions.
    const auto content = readContent(contentReader);

    // Check if there is an workflow for the provided id.
    if (!manager->hasWorkflow(workflowId)) {
      response.status = net::statuscodes::NOT_FOUND;
      return;
    }

    // Empty content is not allowed.
    if (content.empty()) {
      net::ModelError modelError{};
      modelError.addError("", "No application/json provided.");
      badRequest(response, modelError);
      return;
    }

    auto jsonContent = json::parse(content);

    // Go through all inputs.
    for (const auto &input : jsonContent) {

      // Check if the id is malformed.
      if (!input.contains("id") || input["id"].is_null() ||
          input["id"].get<std::string>().empty()) {

        net::ModelError modelError;
        modelError.addError("id", "Id can not be null or empty.");

        badRequest(response, modelError);

        return;
      }

      // Only set a value if there is one.
      if (input.contains("value")) {
        manager->inputInteractionValue(workflowId, input["id"], input["value"]);
      }
    }

    // Return all interactions.
    response.set_content(to_string(manager->getInteractions(workflowId)),
                         net::mimetypes::APPLICATION_JSON);
  }

  void WorkflowController::startWorkflow(
    const httplib::Request &request, httplib::Response &response,
    const httplib::ContentReader &contentReader) {
    std::string workflowFilePath{};
    std::string engineName{};

    // Multipart/form-data.
    if (request.is_multipart_form_data()) {
      auto items = readMultipartFormData(contentReader);

      auto fileIter = items.find("workflow");
      auto engineIter = items.find("engine");

      // The file must be provided.
      if (fileIter == items.end() || fileIter->second.front().content_type !=
                                       net::mimetypes::APLICATION_OCTETSTREAM) {
        net::ModelError modelError;
        modelError.addError("workflow", "Please provide a workflow file.");

        badRequest(response, modelError);
        return;
      }

      // Engine is optional. The default engine is used when no name is provided.
      if (engineIter != items.end()) {
        engineName = engineIter->second.front().content;
      }

      const auto &file = fileIter->second.front();

      // Store the file.
      if (!saveUploadedWorkflow(file.content, workflowFilePath)) {
        response.status = net::statuscodes::INTERNAL_SERVER_ERROR;
        return;
      }
    }
    // application/json
    else if (request.get_header_value("Content-Type") ==
             net::mimetypes::APPLICATION_JSON) {

      const auto content = readContent(contentReader);

      // No JSON provided
      if (content.empty()) {
        net::ModelError modelError;
        modelError.addError("", "No application/json provided.");

        badRequest(response, modelError);
        return;
      }

      auto jsonContent = json::parse(content);

      // Check if there is workflow object in the JSON.
      if (!jsonContent.contains("workflow") ||
          jsonContent["workflow"].is_null()) {
        net::ModelError modelError;
        modelError.addError("workflow", "Please provide a workflow.");

        badRequest(response, modelError);
        return;
      }

      // Name of the used engine is optional.
      if (jsonContent.contains("engine")) {
        engineName = jsonContent["engine"].get<std::string>();
      }

      // Save the workflow as file.
      if (!saveUploadedWorkflow(to_string(jsonContent["workflow"]),
                                workflowFilePath)) {
        response.status = net::statuscodes::INTERNAL_SERVER_ERROR;
        return;
      }
    } else {
      response.status = net::statuscodes::UNSUPPORTED_MEDIA_TYPE;
      return;
    }

    // Start the workflow and return the infos about it.
    response.status = net::statuscodes::CREATED;
    response.set_content(
      to_string(manager->startWorkflow(workflowFilePath, engineName, false)),
      net::mimetypes::APPLICATION_JSON);
  }

  void WorkflowController::continueWorkflow(const httplib::Request &request,
                                            httplib::Response &response) {
    auto workflowId = getIntRouteValue(request, workflowIdRouteValueIndex);

    if (!manager->hasWorkflow(workflowId)) {
      response.status = net::statuscodes::NOT_FOUND;
      return;
    }

    manager->continueWorkflow(workflowId);
    response.set_content(to_string(json{{"state", manager->getWorkflowStatus(
                                                    workflowId)["state"]}}),
                         net::mimetypes::APPLICATION_JSON);
  }

  bool
  WorkflowController::saveUploadedWorkflow(const std::string &workflowContent,
                                           std::string &absoluteFilePath) {

    std::string flowStorageFolder =
      manager->getBasePath() + config::REST_WORKFLOW_FOLDER_NAME;

    absoluteFilePath =
      flowStorageFolder + "/" + util::createRandomFileName(".flow");

    util::createDirectory(flowStorageFolder);

    if (util::fileExists(absoluteFilePath)) { return false; }

    std::ofstream out(absoluteFilePath);
    out << std::setw(4) << workflowContent << std::endl;
    std::cout << "Wrote received json to " << absoluteFilePath << std::endl;

    return true;
  }
} // namespace webapi
