/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "Schemas.h"

namespace webapi::schemas {

  extern const json STARTWORKFLOW_APPLICATION_JSON = R"({
    "type": "object",
    "properties": {
      "engine": {
        "type": "string"
      },
      "workflow": {
        "type": "object"
      }
    },
    "required": ["workflow"]
  })"_json;

  extern const json STARTWORKFLOW_MULTIPART_FORMDATA = R"({
    "type": "object",
    "properties": {
      "engine": {
        "type": "string"
      },
      "workflow": {
        "type": "string",
        "format": "binary"
      }
    },
    "required": ["workflow"]
  })"_json;

  extern const json ENGINE = R"(
{
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "path": {
      "type": "string"
    }
  },
  "required": [
    "name",
    "path"
  ]
}
  )"_json;

  extern const json ENGINES = R"(
{
  "type": "array",
  "items": {
    "$ref": "#/components/schemas/Engine"
  }
}
  )"_json;

  extern const json WORKFLOW = R"(
{
  "type": "object",
  "properties": {
    "endDateTime": {
      "type": "string"
    },
    "fileName": {
      "type": "string"
    },
    "id": {
      "type": "integer"
    },
    "nodesProcessed": {
      "type": "integer"
    },
    "nodesTotal": {
      "type": "integer"
    },
    "processEngine": {
      "type": "string"
    },
    "startDateTime": {
      "type": "string"
    },
    "state": {
      "type": "string"
    }
  },
  "required": [
    "endDateTime",
    "fileName",
    "id",
    "nodesProcessed",
    "nodesTotal",
    "processEngine",
    "startDateTime",
    "state"
  ]
}
  )"_json;

  extern const json WORKFLOWS = R"(
{
  "type": "array",
  "items": {
    "$ref": "#/components/schemas/Workflow"
  }
}
  )"_json;

  extern const json INTERACTION = R"(
{
  "type": "object",
  "properties": {
    "default_value": {
      "type": "string"
    },
    "description": {
      "type": "string"
    },
    "direction": {
      "type": "string"
    },
    "id": {
      "type": "string"
    },
    "order": {
      "type": "integer"
    },
    "pageNumber": {
      "type": "integer"
    },
    "type": {
      "type": "string"
    },
    "value": {
      "type": "string"
    }
  },
  "required": [
    "default_value",
    "description",
    "direction",
    "id",
    "order",
    "pageNumber",
    "type",
    "value"
  ]
}
  )"_json;

  extern const json INTERACTIONS = R"(
{
  "type": "object",
  "properties": {
    "interactions": {
      "type": "array",
      "items":
        {
          "$ref": "#/components/schemas/Interaction"
        }
    }
  }
}
  )"_json;

  extern const json INPUT = R"(
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "value": {
      "type": "string"
    }
  },
  "required": [
    "id",
    "value"
  ]
}
  )"_json;

  extern const json INPUTS = R"(
{
  "type": "array",
  "items":
    {
      "$ref": "#/components/schemas/Input"
    }
}
  )"_json;

  extern const json WORKFLOW_STATE = R"(
{
  "type": "object",
  "properties": {
    "state": {
      "type": "string"
    }
  },
  "required": [
    "state"
  ]
}
  )"_json;

} // namespace webapi::schemas
