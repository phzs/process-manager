/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "ProcessManagerService.h"
#include "EngineController.h"
#include "Schemas.h"
#include "WorkflowController.h"

namespace webapi {

  ProcessManagerService::ProcessManagerService(
    ProcessManager *manager, const net::RestService::Config &config)
      : net::RestService(config) {

    auto &engineTag = addController<EngineController>(manager);
    engineTag.description = "Everything related to process engines.";

    auto &workflowTag = addController<WorkflowController>(manager);
    workflowTag.description = "Everything related to workflows.";

    auto &openApi = getOpenAPI();
    openApi.info.title = "Process-Manager";
    openApi.info.version = PROCESS_MANAGER_VERSION;

    auto &schemas = openApi.components.schemas;
    schemas["Workflow"] = schemas::WORKFLOW;
    schemas["Engine"] = schemas::ENGINE;
    schemas["Interaction"] = schemas::INTERACTION;
    schemas["Input"] = schemas::INPUT;

    addOpenAPIEndpoint("Process-Manager");
  }
} // namespace webapi