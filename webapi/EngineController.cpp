/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "EngineController.h"
#include "../ProcessManager.h"
#include "../net/MimeTypes.h"
#include "../net/Statuscodes.h"
#include "Schemas.h"

namespace webapi {
  EngineController::EngineController(ProcessManager *manager,
                                     httplib::Server *server,
                                     const std::string &basePath)
      : net::ApiController(server, net::UriPath(basePath) / "engines") {

    setName("engine");

    // GET /engines
    auto &info =
      addGetEndpoint(net::UriPath(), [manager](const httplib::Request &request,
                                               httplib::Response &response) {
        response.set_content(to_string(manager->getEnginesList()),
                             net::mimetypes::APPLICATION_JSON);
      });

    info.summary = "Finds all registered process-engines.";
    info.description =
      "Finds all process-engines that are registered in this process-manager.";

    info.responses[net::statuscodes::OK] = {
      "The Engines.",
      {{net::mimetypes::APPLICATION_JSON, schemas::ENGINES}}};
  }
} // namespace webapi
