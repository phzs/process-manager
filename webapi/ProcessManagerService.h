/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../net/RestService.h"

class ProcessManager;

namespace webapi {

  /**
   * @brief The process-manager REST service.
   * 
   */
  class ProcessManagerService : public net::RestService {
  public:
    /**
     * @brief Construct a new ProcessManagerService object.
     * 
     * @param manager The ProcessManager.
     * @param config The config for this REST service.
     */
    ProcessManagerService(ProcessManager *manager,
                          const net::RestService::Config &config);

    /**
     * @brief Destroy the ProcessManagerService object.
     * 
     */
    ~ProcessManagerService() = default;
  };
} // namespace webapi