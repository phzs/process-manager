/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../net/ApiController.h"

class ProcessManager;

namespace webapi {

  /**
   * @brief Controller for the engines endpoints.
   * 
   */
  class EngineController : public net::ApiController {
  public:
    /**
     * @brief Construct a new EngineController object.
     * 
     * @param manager The ProcessManager.
     * @param server The server.
     * @param basePath The path prefix for all endpoints of this controller.
     */
    EngineController(ProcessManager *manager, httplib::Server *server,
                     const std::string &basePath);

    /**
     * @brief Destroy the EngineController object.
     * 
     */
    ~EngineController() = default;
  };

} // namespace webapi