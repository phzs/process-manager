/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#include "process.h"
#include <cstring>
#include <stdexcept>
#include <zconf.h>

namespace process {
  void runDetached(const std::vector<std::string> &command) {
    if (command.empty()) throw std::invalid_argument("Command is empty");
    fflush(nullptr);
    pid_t pid = fork();
    if (pid == -1) {
      throw std::runtime_error("Can not fork child process " +
                               std::string(strerror(errno)));
    } else if (pid == 0) {
      // this is the child process

      // convert command as expected by execv()
      const char *program_name = command[0].c_str();
      const char **argv = new const char *[command.size() + 2];
      argv[0] =
        program_name; // by convention, the first element must be the program name
      int index = 1;
      for (; index < command.size(); ++index) {
        argv[index] = command[index].c_str();
      }
      argv[index] = nullptr; // by convention, the last element must be empty
      argv[index + 1] =
        nullptr; // by convention, the last element must be empty

      /* The standard pipes of the child process must be reassigned, otherwise the pipes of the parent
       * will be inherited and spammed, making the json output of the process manager on stdout unusable.
       * Use /dev/null as "file" because we don't need any output (or input) from the child process */
      const char *target = "/dev/null";
      if (!freopen(target, "r", stdin))
        throw std::runtime_error("redirecting stdin failed");
      if (!freopen(target, "w", stdout))
        throw std::runtime_error("redirecting stdout failed");
      if (!freopen(target, "w", stderr))
        throw std::runtime_error("redirecting stderr failed");

      execvp(program_name, (char **) argv);
      throw std::runtime_error("Execv() failed " +
                               std::string(strerror(errno)));
    }
  }
} // namespace process
