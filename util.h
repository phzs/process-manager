/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <string>
#include <vector>

namespace util {

  /**
   * Check if given file exists on the file system
   * @param fileName Name (incl. path) to check
   * @return true if the file exists
   */
  bool fileExists(const std::string &fileName);


  /** Check if a path is absolute
   * @param path path to be checked
   * @return true if path is absolute
   */
  bool isAbsolutePath(const std::string &path);

  /**
   * Get absolute path for a filename in the same directory (adding cwd)
   * @param fileName
   * @return absolute path
   */
  std::string getAbsolutePath(const std::string &fileName);

  /**
   * Creates the specified directory if it does not exist
   * Also creates non-existent parents (like `mkdir -p`)
   * @param path Path of the directory to create
   * @return true if the directory exists afterwards (was created or existed before)
   */
  bool createDirectory(const std::string &path);

  /**
   * Copies a file to a destination path
   * @param path Path of the file to copy
   * @param path Destination path
   * @return true if the file was copied successfully
   */
  bool copyFile(const std::string &from, const std::string &to);

  /**
   * Convert all letters in a string to lowercase.
   * @param str String to be converted
   * @return lowercase string
   */
  std::string toLower(const std::string &str);

  /**
   * Get current time as a string
   * @param format time format to be used
   * @return current time as string in given format
   */
  std::string currentTime(const std::string &format = "%Y-%m-%dT%H:%M:%S.%z%Z");

  /**
    * Return a hash value of given string.
    * The result will be a numeric hash value returned in form of a string.
    * @param stringToHash the string to be hashed
    * @return the (numeric) hash as a string
    */
  std::string stringHash(const std::string &stringToHash);

  /**
   * Creates a random name for file.
   * 
   * @param fileExtension The file extension to add.
   * @return std::string The random file name.
   */
  std::string createRandomFileName(const std::string &fileExtension);

} // namespace util
