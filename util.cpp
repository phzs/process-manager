/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include <algorithm>
#include <experimental/filesystem>
#include <iomanip>
#include <iostream>
#include <random>
#include <stdexcept>

namespace fs = std::experimental::filesystem;

namespace util {

  bool fileExists(const std::string &fileName) { return fs::exists(fileName); }

  bool isAbsolutePath(const std::string &path) {
    fs::path toCheck = path;
    return toCheck.is_absolute();
  }

  std::string getAbsolutePath(const std::string &fileName) {
    return fs::absolute(fileName);
  }

  bool createDirectory(const std::string &path) {
    bool result = false;

    if (!fs::is_directory(path) || !fs::exists(path)) {
      try {
        result = fs::create_directories(path); // creates non-existing parents
      } catch (fs::filesystem_error &error) {
        std::cerr << error.what() << std::endl;
        result = false;
      }
    }
    return result;
  }

  bool copyFile(const std::string &from, const std::string &to) {
    return fs::copy_file(from, to);
  }

  std::string toLower(const std::string &str) {
    std::string result = str;
    std::transform(result.begin(), result.end(), result.begin(), ::tolower);
    return result;
  }

  std::string currentTime(const std::string &format) {
    std::stringstream result;
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    result << std::put_time(&tm, format.c_str());
    return result.str();
  }

  std::string stringHash(const std::string &str) {
    std::hash<std::string> stringHash;
    return std::to_string(stringHash(str));
  }

  std::string createRandomFileName(const std::string &fileExtension) {
    std::string timestamp = currentTime("%Y-%m-%dT%H:%M:%S.%z%Z");
    std::random_device randomDevice;
    std::mt19937 randomEngine(randomDevice());
    std::uniform_int_distribution<int> randomDistribution(
      1, std::numeric_limits<int>::max());
    auto const randomNumber = randomDistribution(randomEngine);
    std::string fileName =
      timestamp + "_" +
      util::stringHash(timestamp + std::to_string(randomNumber)) +
      fileExtension;
    return fileName;
  }

} // namespace util
