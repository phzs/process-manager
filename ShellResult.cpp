/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "ShellResult.h"
#include <iostream>

json ShellResult::parseStdoutAsJson() const {

  if (stdout_raw.empty()) { return {}; }

  std::string processEngineStdout = stdout_raw;

  // replace all new lines to avoid parsing errors:
  std::replace(processEngineStdout.begin(), processEngineStdout.end(), '\n',
               ' ');

  json result;
  try {
    result = json::parse(processEngineStdout);
  } catch (json::parse_error &error) {
    std::ostringstream logStream;
    logStream << "error parsing process engine response: " << error.what()
              << std::endl;
    std::cerr << logStream.str();
  }
  return result;
}
