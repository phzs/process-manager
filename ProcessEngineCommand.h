/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "ProcessManager.h"
#include "config.h"
#include <nlohmann/json.hpp>
#include <string>

using json = nlohmann::json;

struct ShellResult;

class ProcessEngineCommand {

public:
  ProcessEngineCommand(std::string path, const std::string &subcommand);
  void addArgument(std::string argument);

  /**
   * Execute the command. This will block until the result (consisting of stdout, stderr and exit_code) is obtained.
   */
  ShellResult execute(bool ignoreTimeout = false);

  /**
   * Execute the command without waiting for a result.
   */
  void executeNonBlocking();

private:
  std::string path;
  [[nodiscard]] std::vector<std::string> getFullCommand() const;
  [[nodiscard]] std::string getFullCommandString() const;
  std::vector<std::string> arguments;
};
