/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../domain/Serializable.h"
#include <unordered_map>
#include <vector>

namespace net {

  /**
   * @brief Stores a number of errors for input fields.
   * Is used as return type for HTTP-400 (BadRequest) responses.
   * 
   */
  class ModelError : public Serializable {
  public:
    /**
     * @brief Construct a new ModelError object.
     * 
     */
    ModelError() = default;

    /**
     * @brief Destroy the ModelError object.
     * 
     */
    ~ModelError() = default;

    /**
     * @brief Adds an error.
     * Multiple errors per key can be added.
     * 
     * @param key The name of the field that is invalid.
     * @param message The error message.
     */
    void addError(const std::string &key, const std::string &message);

    /**
     * @brief Indicates if the any errors have been added.
     * 
     * @return true There are errors added.
     * @return false No errors added.
     */
    bool hasErrors() const;

    /**
     * @brief Converts the errors to JSON.
     * 
     * @return json The JSON.
     */
    json getJson() const override;

    /**
     * @brief Restores errors from JSON.
     * 
     * @param object A ModelError serialized to JSON.
     */
    void fromJson(json object) override;

  private:
    using Errors = std::unordered_map<std::string, std::vector<std::string>>;
    Errors errors;
  };

} // namespace net