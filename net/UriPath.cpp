/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "UriPath.h"
#include "../util.h"
#include <iostream>

namespace net {

  UriPath::UriPath() : lastChar(' ') {}

  UriPath::UriPath(const std::string &path) {

    // Missing '/' at the beginning.
    // Also added when the path is empty.
    if (path.front() != '/') { pathStream << '/'; }

    pathStream << path;

    if (path.empty()) {
      // If the path is empty, at least the '/' is added to the stream.
      lastChar = '/';
    } else {
      lastChar = path.back();
    }
  }

  UriPath::UriPath(UriPath &&uriPath) noexcept
      : pathStream(std::move(uriPath.pathStream)), lastChar(uriPath.lastChar) {}

  UriPath &UriPath::operator/(const std::string &pathSegment) {

    // Avoid that duplicate '/' is inserted.
    if (lastChar != '/') { pathStream << '/'; }

    if (!pathSegment.empty()) { pathStream << pathSegment; }

    lastChar = pathSegment.back();

    return *this;
  }

  UriPath &UriPath::operator/(const UriPath &uriPath) {

    auto path = uriPath.toString();

    // Avoid duplicate '/'.
    if (lastChar == '/') {

      // An UriPath starts always with a '/' at the beginning, remove it.
      path.erase(0, 1);
      pathStream << path;
    } else {
      pathStream << path;
    }

    lastChar = path.back();

    return *this;
  }

  std::string UriPath::toString() const { return pathStream.str(); }
} // namespace net
