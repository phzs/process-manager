/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "RouteParam.h"
#include <iostream>
#include <regex>

namespace net::routeparam {
  extern const std::string INTEGER{R"((\d+))"};

  std::string create(const std::string &name, const std::string &type) {
    return "{" + name + ":" + type + "}";
  }

  std::string transformForHttpLib(const std::string &path) {
    static const std::regex intValue("\\{(\\w+):(integer)\\}");

    return std::regex_replace(path, intValue, routeparam::INTEGER);
  }

  std::string transformForOpenAPI(const std::string &path) {

    static const std::regex routeValue("(\\{(\\w+):(\\w+)\\})");
    return std::regex_replace(path, routeValue, "{$2}");
    ;
  }
} // namespace net::routeparam