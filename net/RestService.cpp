/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "RestService.h"
#include "MimeTypes.h"
#include "Schemas.h"
#include "openapi/OpenAPIController.h"
#include <cpp-httplib/httplib.h>

#include <utility>

namespace net {
  RestService::RestService(const Config &config) : config(config) {

    // Initialize the correct server based on the config.
    if (config.useSsl) {
      server = std::make_unique<httplib::SSLServer>(
        config.x509Path.c_str(), config.privateKeyPath.c_str());
    } else {
      std::cout << "WARNING: You are not using SSL." << std::endl;
      server = std::make_unique<httplib::Server>();
    }

    schema = config.useSsl ? "https" : "http";

    // Add an exception handler.
    server->set_exception_handler(
      [](const auto &request, auto &response, std::exception_ptr eptr) {
        response.status = 500;

        ModelError modelError{};
        try {
          std::rethrow_exception(std::move(eptr));
        } catch (std::exception &e) {
          modelError.addError("", e.what());
        } catch (...) { modelError.addError("", "Unknown Exception"); }
        response.set_content(to_string(modelError.getJson()),
                             mimetypes::APPLICATION_JSON);
      });
  }

  void RestService::serve() {

    std::cout << "Serve API at " << schema << "://" << config.host << ":"
              << config.port << UriPath(config.endpoint).toString()
              << std::endl;
    server->listen(config.host, (int) config.port);
  }

  openapi::OpenAPI &RestService::getOpenAPI() { return openApi; }

  void RestService::addOpenAPIEndpoint(const std::string &serverDescription) {

    openapi::Server serverInfo{.url = schema + "://" + config.host + ":" +
                                      std::to_string(config.port),
                               .description = serverDescription};

    openApi.servers.push_back(serverInfo);

    // Add the schemas that are common for all REST services.
    openApi.components.schemas["ModelError"] = schemas::MODEL_ERROR;

    json result = openApi;

    // Add the controller for OpenAPI.
    controllers.push_back(std::make_unique<openapi::OpenAPIController>(
      to_string(result), server.get(), config.endpoint));
  }
} // namespace net
