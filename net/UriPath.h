/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <sstream>
#include <string>

namespace net {

  /**
   * @brief Represents the path part of an uri.
   * 
   */
  class UriPath {
  public:
    /**
     * @brief Construct a new UriPath object.
     * 
     */
    UriPath();

    /**
     * @brief Construct a new UriPath object.
     * @param path An already existing path.
     */
    explicit UriPath(const std::string &path);

    /**
     * @brief Construct a new UriPath object.
     * @param path An other UriPath object.
     */
    UriPath(const UriPath &uriPath) = delete;

    /**
     * @brief Construct a new UriPath object.
     * @param uriPath An other UriPath object.
     */
    UriPath(UriPath &&uriPath) noexcept;

    /**
     * @brief Destroy the UriPath object.
     */
    ~UriPath() = default;

    /**
     * @brief Adds a path segment at the end of the current path.
     * @param pathSegment The path segment that should be added.
     * @return UriPath& The UriPath that allows to concatenate multiple path segments. 
     */
    UriPath &operator/(const std::string &pathSegment);

    /**
     * @brief Adds an existing UriPath at the end of the current path.
     * @param uriPath The UriPath.
     * @return UriPath& The UriPath that allows to concatenate multiple path segments. 
     */
    UriPath &operator/(const UriPath &uriPath);

    /**
     * @brief Converts the uri path to string.
     * @return std::string The path.
     */
    std::string toString() const;

  private:
    std::stringstream pathStream;
    char lastChar;
  };
} // namespace net
