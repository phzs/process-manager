/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "ApiController.h"
#include "openapi/OpenAPI.h"
#include <memory>
#include <string>
#include <unordered_map>

namespace httplib {
  class Server;
  class ContentReader;
  class Request;
} // namespace httplib

namespace net {
  /**
   * @brief Base class for all REST services.
   * 
   */
  class RestService {
  public:
    /**
     * @brief Config for a REST service.
     * 
     */
    struct Config {
      std::string endpoint;
      std::string host;
      unsigned int port;
      bool useSsl;
      std::string x509Path;
      std::string privateKeyPath;
    };

  public:
    /**
     * @brief Construct a new RestService object.
     * 
     * @param config The config.
     */
    explicit RestService(const Config &config);

    /**
     * @brief Destroy the RestService object.
     * 
     */
    ~RestService() = default;

    /**
     * @brief Starts the service.
     * 
     */
    void serve();

    /**
     * @brief Returns the OpenAPI documentation.
     * 
     * @return openapi::OpenAPI& The documentation.
     */
    openapi::OpenAPI &getOpenAPI();

  protected:
    /**
     * @brief Convenience function to add a controller to the service.
     * 
     * @tparam TController The type of the controller.
     * @tparam Args Type of arguments.
     * @param args The arguments that should be forwarded to the constructor.
     * @return openapi::Tag& An OpenAPI tag object.
     */
    template<typename TController, typename... Args>
    openapi::Tag &addController(Args &&...args) {
      controllers.push_back(std::make_unique<TController>(
        std::forward<Args>(args)..., server.get(), config.endpoint));

      auto controller = controllers.back().get();

      openapi::Tag tag;
      tag.name = controller->getName();
      tag.description = "";

      openApi.tags.push_back(tag);

      auto controllerPaths = controller->getOpenAPIPaths();
      openApi.paths.insert(openApi.paths.end(), controllerPaths.begin(),
                           controllerPaths.end());

      return openApi.tags.back();
    }

    /**
     * @brief Adds an OpenAPI endpoint to request the OpenAPI specification of this service.
     * 
     * @param serverDescription The description of the server.
     */
    void addOpenAPIEndpoint(const std::string &serverDescription);

  protected:
    std::unique_ptr<httplib::Server> server;
    std::vector<std::unique_ptr<ApiController>> controllers;
    Config config;

  private:
    std::string schema;
    openapi::OpenAPI openApi;
  };
} // namespace net
