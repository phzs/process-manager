/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "OpenAPI.h"
#include <string>

namespace net::openapi {

  /**
   * @brief Builder that helps to construct OpenAPI paths.
   * 
   */
  class OpenAPIPathsBuilder {
  public:
    /**
     * @brief Construct a new OpenAPIPathsBuilder object.
     * 
     */
    OpenAPIPathsBuilder() = default;

    /**
     * @brief Destroy the OpenAPIPathsBuilder object.
     * 
     */
    ~OpenAPIPathsBuilder() = default;

    /**
     * @brief Builds all added paths.
     * 
     * @return std::vector<Path> The paths.
     */
    std::vector<Path> build() const;

    /**
     * @brief Sets the tag that is used to group all the added endpoints.
     * 
     * @param tagName The name of the tag.
     */
    void setTagName(const std::string &tagName);

    /**
     * @brief Adds a HTTP-Get operation.
     * 
     * @param path The path.
     * @return Operation& An OpenAPI operation object.
     */
    Operation &addGetOperation(const std::string &path);

    /**
     * @brief Adds a HTTP-POST operation.
     * 
     * @param path The path.
     * @return Operation& An OpenAPI operation object.
     */
    Operation &addPostOperation(const std::string &path);

    /**
     * @brief Adds a HTTP-PUT operation.
     * 
     * @param path The path.
     * @return Operation& An OpenAPI operation object.
     */
    Operation &addPutOperation(const std::string &path);

    /**
     * @brief Adds a HTTP-PATCH operation.
     * 
     * @param path The path.
     * @return Operation& An OpenAPI operation object.
     */
    Operation &addPatchOperation(const std::string &path);

  private:
    void addPathParameters(Operation &operation, const std::string &path);

    Operation &addOperation(const std::string &method, const std::string &path);

  private:
    std::string tagName;
    std::map<std::string, openapi::Path> paths;
  };
} // namespace net::openapi