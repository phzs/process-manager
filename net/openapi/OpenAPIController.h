/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../ApiController.h"

namespace net::openapi {

  /**
   * @brief 
   * 
   */
  class OpenAPIController : public ApiController {
  public:
    /**
     * @brief Construct a new OpenAPIController object.
     * 
     * @param openApiDoc The OpenAPI documentation.
     * @param server The server.
     * @param basePath The base path for the endpoint.
     */
    OpenAPIController(const std::string &openApiDoc, httplib::Server *server,
                      const std::string &basePath);

    /**
     * @brief Destroy the OpenAPIController object.
     * 
     */
    ~OpenAPIController() = default;
  };

} // namespace net::openapi