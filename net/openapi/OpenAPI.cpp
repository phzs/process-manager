/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "OpenAPI.h"
#include <iostream>

namespace net::openapi {

  void addSchema(json &j, const json &schema) {

    if (schema.is_null()) { return; }

    // We assume that objects are inline schemas and a string is a link.
    if (schema.is_object()) {
      j["schema"] = schema;
    } else if (schema.is_string()) {
      j["schema"]["$ref"] = schema;
    } else {
      throw std::logic_error("Unable to determine the schema of the request "
                             "body. Use a json object to provide an inline "
                             "schema or a string to link to a schema.");
    }
  }

  void addContent(json &j, const std::map<std::string, json> &content) {

    if (content.size() == 0) { return; }

    json result;

    // We insert the schema directly instead of using a MediaType object.
    for (const auto &pair : content) {
      addSchema(result[pair.first], pair.second);
    }

    j["content"] = result;
  }

  void to_json(json &j, const Server &server) {
    j = json{{"url", server.url}, {"description", server.description}};
  }

  void to_json(json &j, const Info &info) {
    j = json{{"title", info.title}, {"version", info.version}};
  }

  void to_json(json &j, const Response &response) {
    j = json{{"description", response.description}};

    addContent(j, response.content);
  }

  void to_json(json &j, const RequestBody &requestBody) {

    j = json{{"description", requestBody.description}};
    addContent(j, requestBody.content);
  }

  void to_json(json &j, const Parameter &parameter) {
    j = json{{"name", parameter.name},
             {"in", parameter.in},
             {"description", parameter.description},
             {"required", parameter.required},
             {"schema", parameter.schema}};
  }

  void to_json(json &j, const Operation &operation) {

    json responses;

    for (const auto &response : operation.responses) {
      responses[std::to_string(response.first)] = response.second;
    }

    j = json{{"tags", operation.tags},
             {"summary", operation.summary},
             {"description", operation.description},
             {"parameters", operation.parameters},
             {"responses", responses},
             {"deprecated", operation.deprecated}};

    if (operation.requestBody.content.size() != 0) {
      j["requestBody"] = operation.requestBody;
    }
  }

  void to_json(json &j, const Path &path) {

    for (const auto &operation : path.operations) {
      j[operation.first] = operation.second;
    }
  }

  void to_json(json &j, const Tag &tag) {

    j = json{{"name", tag.name}, {"description", tag.description}};
  }

  void to_json(json &j, const Component &component) {

    j = json{{"schemas", component.schemas}};
  }

  void to_json(json &j, const OpenAPI &openApi) {

    json pathsJson{};

    for (const auto &path : openApi.paths) {
      pathsJson[path.relativeUrl] = path;
    }

    j = json{{"openapi", openApi.openapi}, {"info", openApi.info},
             {"servers", openApi.servers}, {"paths", pathsJson},
             {"tags", openApi.tags},       {"components", openApi.components}};
  }

  std::string refToSchema(const std::string &schemaName) {
    return "#/components/schemas/" + schemaName;
  }
} // namespace net::openapi