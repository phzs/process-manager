/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../../domain/Serializable.h"
#include <string>
#include <vector>

namespace net::openapi {

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#infoObject
  struct Info {
    std::string title;
    std::string version;
  };

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#serverObject
  struct Server {
    std::string url;
    std::string description;
  };

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#responsesObject
  struct Response {
    std::string description;
    // Content is normally a map of string and MediaType:
    // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#mediaTypeObject
    // But at the moment we only need the json schema from the MediaType.
    std::map<std::string, json> content;
  };

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#requestBodyObject
  struct RequestBody {
    std::string description;

    // Content is normally a map of string and MediaType:
    // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#mediaTypeObject
    // But at the moment we only need the json schema from the MediaType..
    std::map<std::string, json> content;
    bool required;
  };

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#parameterObject
  struct Parameter {
    std::string name;
    std::string in;
    std::string description;
    bool required;
    json schema;
  };

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#operationObject
  struct Operation {
    std::vector<std::string> tags;
    std::string summary;
    std::string description;
    std::vector<Parameter> parameters;
    RequestBody requestBody;
    std::map<int, Response> responses;
    bool deprecated{false};
  };

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#pathItemObject
  struct Path {
    std::string relativeUrl;
    // We don't model the operations explicitly like in the schema.
    // With a map it is easier to check which operations are not used without
    // making the usage of pointers and checking for nullptr.
    std::map<std::string, Operation> operations;
  };

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#tagObject
  struct Tag {
    std::string name;
    std::string description;
  };

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#componentsObject
  struct Component {
    std::map<std::string, json> schemas;
  };

  // https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#schema
  struct OpenAPI {
    std::string openapi{"3.0.0"};
    Info info;
    std::vector<Server> servers;
    std::vector<Path> paths;
    std::vector<Tag> tags;
    Component components;
  };

  /**
   * @brief Converts an OpenAPI object to JSON.
   * 
   * @param j The JSON object to store the output.
   * @param openApi The OpenAPI object.
   */
  void to_json(json &j, const OpenAPI &openApi);

  /**
   * @brief Creates a reference to a schema that is defined in the OpenAPI components/schemas section.
   * The reference has the format: #/components/schemas/<schemaName>
   * 
   * @param schemaName The name of the schema.
   * @return std::string The reference.
   */
  std::string refToSchema(const std::string &schemaName);
} // namespace net::openapi