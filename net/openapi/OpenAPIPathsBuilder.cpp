/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "OpenAPIPathsBuilder.h"
#include "../MimeTypes.h"
#include "../RouteParam.h"
#include "../Schemas.h"
#include <regex>

namespace net::openapi {

  std::vector<openapi::Path> OpenAPIPathsBuilder::build() const {
    std::vector<openapi::Path> transformedPaths;

    for (auto path : paths) { transformedPaths.push_back(path.second); }

    return transformedPaths;
  }

  void OpenAPIPathsBuilder::setTagName(const std::string &tagName) {
    this->tagName = tagName;
  }

  Operation &OpenAPIPathsBuilder::addGetOperation(const std::string &path) {
    return addOperation("get", path);
  }

  Operation &OpenAPIPathsBuilder::addPostOperation(const std::string &path) {
    return addOperation("post", path);
  }

  Operation &OpenAPIPathsBuilder::addPutOperation(const std::string &path) {
    return addOperation("put", path);
  }

  Operation &OpenAPIPathsBuilder::addPatchOperation(const std::string &path) {
    return addOperation("patch", path);
  }

  void OpenAPIPathsBuilder::addPathParameters(Operation &operation,
                                              const std::string &path) {

    static const std::regex routeValueRegex("(\\{(\\w+):(\\w+)\\})");

    // Iterate through all matches.
    for (auto iter =
           std::sregex_iterator(path.begin(), path.end(), routeValueRegex);
         iter != std::sregex_iterator(); ++iter) {
      std::smatch match = *iter;

      Parameter parameter;
      parameter.name = match[2];

      // The parameter is in the path, so it is required.
      parameter.required = true;
      parameter.in = "path";
      parameter.schema = json{{"type", match[3]}};

      operation.parameters.push_back(parameter);
    }
  }

  Operation &OpenAPIPathsBuilder::addOperation(const std::string &method,
                                               const std::string &path) {

    Operation operation{};
    // Always create a '200' response object, as one response is always required.
    operation.responses[200] = {"Successfull response."};

    // Internal server error can always happen.
    // Return the exception also as ModelError to create a uniform response.
    operation.responses[500] = {
      "Internal Server Error.",
      {{mimetypes::APPLICATION_JSON, refToSchema("ModelError")}}};

    operation.tags.push_back(tagName);

    // Parse the path to extract all parameters.
    addPathParameters(operation, path);

    // We have to remove the type.
    auto openApiPath = routeparam::transformForOpenAPI(path);

    auto iter = paths.find(openApiPath);
    if (iter == paths.end()) { paths[openApiPath].relativeUrl = openApiPath; }

    // Group all operations with the same path.
    paths[openApiPath].operations[method] = operation;

    // Return the operation to allow adding some extra infos.
    return paths[openApiPath].operations.at(method);
  }

} // namespace net::openapi