/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "ModelError.h"
#include "UriPath.h"
#include "openapi/OpenAPI.h"
#include "openapi/OpenAPIPathsBuilder.h"
#include <cpp-httplib/httplib.h>
#include <map>
#include <nlohmann/json.hpp>
#include <sstream>
#include <string>

using json = nlohmann::json;

namespace net {
  using MultipartFormDataMap =
    std::unordered_map<std::string, std::vector<httplib::MultipartFormData>>;

  /**
   * @brief Base class for all controllers of a REST service.
   * 
   */
  class ApiController {
  public:
    /**
     * @brief Construct a new ApiController object.
     * 
     * @param server The server.
     * @param basePath The base path used to prefix all endpoints of this controller.
     */
    ApiController(httplib::Server *server, const UriPath &basePath);

    /**
     * @brief Destroy the ApiController object.
     * 
     */
    ~ApiController() = default;

    /**
     * @brief Adds a HTTP-GET endpoint.
     * 
     * @param path The path of the endpoint.
     * @param handler The handler to process incoming requests.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    openapi::Operation &addGetEndpoint(const UriPath &path,
                                       httplib::Server::Handler handler);

    /**
     * @brief Adds a HTTP-POST endpoint.
     * 
     * @param path The path of the endpoint.
     * @param handler The handler to process incoming requests.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    openapi::Operation &
    addPostEndpoint(const UriPath &path,
                    httplib::Server::HandlerWithContentReader handler);

    /**
     * @brief Adds a HTTP-PUT endpoint.
     * 
     * @param path The path of the endpoint.
     * @param handler The handler to process incoming requests.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    openapi::Operation &addPutEndpoint(const UriPath &path,
                                       httplib::Server::Handler handler);

    /**
     * @brief Adds a HTTP-PUT endpoint.
     * 
     * @param path The path of the endpoint.
     * @param handler The handler to process incoming requests.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    openapi::Operation &
    addPutEndpointWithReader(const UriPath &path,
                             httplib::Server::HandlerWithContentReader handler);

    /**
     * @brief Adds a HTTP-PATCH endpoint.
     * 
     * @param path The path of the endpoint.
     * @param handler The handler to process incoming requests.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    openapi::Operation &addPatchEndpoint(const UriPath &path,
                                         httplib::Server::Handler handler);

    /**
     * @brief Adds a HTTP-PATCH endpoint.
     * 
     * @param path The path of the endpoint.
     * @param handler The handler to process incoming requests.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    openapi::Operation &addPatchEndpointWithReader(
      const UriPath &path, httplib::Server::HandlerWithContentReader handler);

    /**
     * @brief Convenience function to add a HTTP-GET endpoint.
     * Uses std::bind to bind a function to an endpoint.
     * 
     * @tparam TController Type of the controller.
     * @tparam THandler Type of the function that handles the request.
     * @param path The path of the endpoint.
     * @param controller The controller.
     * @param handler The handler.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    template<typename TController, typename THandler>
    openapi::Operation &addGetEndpoint(const UriPath &path,
                                       TController *controller,
                                       THandler &&handler) {
      return addGetEndpoint(path, std::bind(handler, controller,
                                            std::placeholders::_1,
                                            std::placeholders::_2));
    }

    /**
     * @brief Convenience function to add a HTTP-POST endpoint.
     * Uses std::bind to bind a function to an endpoint.
     * 
     * @tparam TController Type of the controller.
     * @tparam THandler Type of the function that handles the request.
     * @param path The path of the endpoint.
     * @param controller The controller.
     * @param handler The handler.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    template<typename TController, typename THandler>
    openapi::Operation &addPostEndpoint(const UriPath &path,
                                        TController *controller,
                                        THandler &&handler) {
      return addPostEndpoint(
        path, std::bind(handler, controller, std::placeholders::_1,
                        std::placeholders::_2, std::placeholders::_3));
    }

    /**
     * @brief Convenience function to add a HTTP-PUT endpoint.
     * Uses std::bind to bind a function to an endpoint.
     * 
     * @tparam TController Type of the controller.
     * @tparam THandler Type of the function that handles the request.
     * @param path The path of the endpoint.
     * @param controller The controller.
     * @param handler The handler.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    template<typename TController, typename THandler>
    openapi::Operation &addPutEndpoint(const UriPath &path,
                                       TController *controller,
                                       THandler &&handler) {
      return addPutEndpoint(path, std::bind(handler, controller,
                                            std::placeholders::_1,
                                            std::placeholders::_2));
    }

    /**
     * @brief Convenience function to add a HTTP-PUT endpoint.
     * Uses std::bind to bind a function to an endpoint.
     * 
     * @tparam TController Type of the controller.
     * @tparam THandler Type of the function that handles the request.
     * @param path The path of the endpoint.
     * @param controller The controller.
     * @param handler The handler.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    template<typename TController, typename THandler>
    openapi::Operation &addPutEndpointWithReader(const UriPath &path,
                                                 TController *controller,
                                                 THandler &&handler) {
      return addPutEndpointWithReader(
        path, std::bind(handler, controller, std::placeholders::_1,
                        std::placeholders::_2, std::placeholders::_2));
    }

    /**
     * @brief Convenience function to add a HTTP-PATCH endpoint.
     * Uses std::bind to bind a function to an endpoint.
     * 
     * @tparam TController Type of the controller.
     * @tparam THandler Type of the function that handles the request.
     * @param path The path of the endpoint.
     * @param controller The controller.
     * @param handler The handler.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    template<typename TController, typename THandler>
    openapi::Operation &addPatchEndpoint(const UriPath &path,
                                         TController *controller,
                                         THandler &&handler) {
      return addPatchEndpoint(path, std::bind(handler, controller,
                                              std::placeholders::_1,
                                              std::placeholders::_2));
    }

    /**
     * @brief Convenience function to add a HTTP-PATCH endpoint.
     * Uses std::bind to bind a function to an endpoint.
     * 
     * @tparam TController Type of the controller.
     * @tparam THandler Type of the function that handles the request.
     * @param path The path of the endpoint.
     * @param controller The controller.
     * @param handler The handler.
     * @return openapi::Operation& An OpenAPI operation object.
     */
    template<typename TController, typename THandler>
    openapi::Operation &addPatchEndpointWithReader(const UriPath &path,
                                                   TController *controller,
                                                   THandler &&handler) {
      return addPatchEndpointWithReader(
        path, std::bind(handler, controller, std::placeholders::_1,
                        std::placeholders::_2, std::placeholders::_3));
    }

    /**
     * @brief Returns all paths added to this controller.
     * 
     * @return std::vector<openapi::Path> The OpenAPI paths.
     */
    std::vector<openapi::Path> getOpenAPIPaths() const;

    /**
     * @brief Sets the name of the controller.
     * Used to create OpenAPI tags.
     * 
     * @param controllerName The name.
     */
    void setName(const std::string &controllerName);

    /**
     * @brief Returns the controller name.
     * 
     * @return std::string The name.
     */
    std::string getName() const { return name; }

  protected:
    /**
     * @brief Helper function to create a 400-response.
     * 
     * @param response The response to use.
     * @param modelError The errors that should be added to the response.
     */
    void badRequest(httplib::Response &response, const ModelError &modelError);

    /**
     * @brief Returns the value of a route parameter as int.
     * 
     * @param request The request.
     * @param index The index of the route parameter.
     * @return int The value.
     */
    int getIntRouteValue(const httplib::Request &request, int index);

    /**
     * @brief Reads the whole content of a request.
     * 
     * @param contentReader The reader.
     * @return std::string The content of the request.
     */
    std::string readContent(const httplib::ContentReader &contentReader);

    /**
     * @brief Reads multipart/form-data from a request.
     * 
     * @param contentReader The reader.
     * @return MultipartFormDataMap The content of the request.
     */
    MultipartFormDataMap
    readMultipartFormData(const httplib::ContentReader &contentReader);

  private:
    httplib::Server *server;
    std::string basePath;
    std::string name;

    openapi::OpenAPIPathsBuilder pathsBuilder;
  };
} // namespace net