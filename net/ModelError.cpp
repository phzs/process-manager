/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "ModelError.h"
#include <iomanip>
#include <iostream>

namespace net {

  void ModelError::addError(const std::string &key,
                            const std::string &message) {
    errors[key].push_back(message);
  }

  bool ModelError::hasErrors() const { return errors.size() != 0; }

  json ModelError::getJson() const {
    json object;

    object["errors"] = errors;

    return object;
  }

  void ModelError::fromJson(json object) {
    if (object.empty() || object == nullptr) {
      std::string error_message = "Unable to parse model error json: ";
      std::cerr << error_message << std::setw(4) << object << std::endl;
      throw std::logic_error(error_message);
    }

    errors = object["errors"].get<Errors>();
  }
} // namespace net