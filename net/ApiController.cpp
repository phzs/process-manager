/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "ApiController.h"
#include "MimeTypes.h"
#include "RouteParam.h"
#include "Statuscodes.h"

namespace net {

  ApiController::ApiController(httplib::Server *server, const UriPath &basePath)
      : server(server), name("ApiController"), basePath(basePath.toString()) {}

  openapi::Operation &
  ApiController::addGetEndpoint(const UriPath &path,
                                httplib::Server::Handler handler) {
    auto fullPath = (UriPath(basePath) / path).toString();

    server->Get(routeparam::transformForHttpLib(fullPath), handler);
    return pathsBuilder.addGetOperation(fullPath);
  }

  openapi::Operation &ApiController::addPostEndpoint(
    const UriPath &path, httplib::Server::HandlerWithContentReader handler) {
    auto fullPath = (UriPath(basePath) / path).toString();

    server->Post(routeparam::transformForHttpLib(fullPath), handler);
    return pathsBuilder.addPostOperation(fullPath);
  }

  openapi::Operation &
  ApiController::addPutEndpoint(const UriPath &path,
                                httplib::Server::Handler handler) {
    auto fullPath = (UriPath(basePath) / path).toString();

    server->Put(routeparam::transformForHttpLib(fullPath), handler);
    return pathsBuilder.addPutOperation(fullPath);
  }

  openapi::Operation &ApiController::addPutEndpointWithReader(
    const UriPath &path, httplib::Server::HandlerWithContentReader handler) {
    auto fullPath = (UriPath(basePath) / path).toString();

    server->Put(routeparam::transformForHttpLib(fullPath), handler);
    return pathsBuilder.addPutOperation(fullPath);
  }

  openapi::Operation &
  ApiController::addPatchEndpoint(const UriPath &path,
                                  httplib::Server::Handler handler) {
    auto fullPath = (UriPath(basePath) / path).toString();

    server->Patch(routeparam::transformForHttpLib(fullPath), handler);
    return pathsBuilder.addPatchOperation(fullPath);
  }

  openapi::Operation &ApiController::addPatchEndpointWithReader(
    const UriPath &path, httplib::Server::HandlerWithContentReader handler) {
    auto fullPath = (UriPath(basePath) / path).toString();

    server->Patch(routeparam::transformForHttpLib(fullPath), handler);
    return pathsBuilder.addPatchOperation(fullPath);
  }

  void ApiController::setName(const std::string &controllerName) {
    name = controllerName;
    pathsBuilder.setTagName(controllerName);
  }

  std::vector<openapi::Path> ApiController::getOpenAPIPaths() const {
    return pathsBuilder.build();
  }

  void ApiController::badRequest(httplib::Response &response,
                                 const ModelError &modelError) {
    response.status = statuscodes::BAD_REQUEST;
    response.set_content(to_string(modelError.getJson()),
                         mimetypes::APPLICATION_JSON);
  }

  int ApiController::getIntRouteValue(const httplib::Request &request,
                                      int index) {
    return std::stoi(request.matches[index]);
  }

  std::string
  ApiController::readContent(const httplib::ContentReader &contentReader) {
    std::string body;

    contentReader([&](const char *data, size_t dataLength) {
      body.append(data, dataLength);
      return true;
    });

    return body;
  }

  MultipartFormDataMap ApiController::readMultipartFormData(
    const httplib::ContentReader &contentReader) {

    std::string lastItem{};
    MultipartFormDataMap items;

    contentReader(
      [&](const httplib::MultipartFormData &file) {
        items[file.name].push_back(file);
        lastItem = file.name;
        return true;
      },
      [&](const char *data, size_t dataLength) {
        items.at(lastItem).back().content.append(data, dataLength);
        return true;
      });

    return items;
  }
} // namespace net
