#!/bin/bash
set -e # stop on errors
BUILD_FOLDER="build/"

mkdir -p "$BUILD_FOLDER"
cmake -B build/ -S .
cmake --build build/
( cd build && cpack )

echo
echo "DEB content:"
echo "------------------------------"
# shellcheck disable=SC2012
DEB=$(ls -t "$BUILD_FOLDER"/*.deb | head -1)
dpkg -I "$DEB"
dpkg -c "$DEB"
