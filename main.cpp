/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "ProcessManager.h"
#include "util.h"
#include "webapi/ProcessManagerService.h"
#include <CLI/CLI.hpp>

int main(int argc, char **argv) {
  CLI::App app{"process_manager: A tool to manage workflow execution using "
               "compatible process engines."};
  app.set_version_flag("--version", PROCESS_MANAGER_VERSION);

  std::string path = getenv("HOME");
  path += "/";
  path += ".process_manager/";
  if (!util::fileExists(path) && !util::createDirectory(path)) {
    throw std::logic_error("Unable to create directory " + path);
  }

  std::string workflowFile, workflowEngine, inputValue;
  int workflowId = 0;
  unsigned int servePort = 9080;
  std::string serveEndpoint = "/";
  std::string serveHost = "localhost";
  bool serveUseSsl{false};
  std::string serveX509Path;
  std::string servePrivateKeyPath;
  std::string interactionId;
  bool forceRefresh = false, noColors = false;

  app.add_option("-p,--path", path,
                 "Base directory to execute in (application needs write "
                 "permissions here). Defaults to $HOME/.process_manager");
  //app.add_flag("-m,--machine-readable", machineReadableOutput, "Give any output in a machine readable format (json)");
  app.require_subcommand(1);

  CLI::App *listCommand = app.add_subcommand(
    "list", "List workflow executions or available workflow engines");
  CLI::App *listWorkflowsCommand = listCommand->add_subcommand(
    "workflows", "List workflow executions and execution history");
  CLI::App *listEnginesCommand =
    listCommand->add_subcommand("engines", "List available process engines");
  listCommand->require_subcommand(1); // one subcommand must be given

  CLI::App *statusCommand = app.add_subcommand(
    "status", "Print status of the workflow execution with the given id");
  statusCommand
    ->add_option("workflowId", workflowId, "Id of the workflow execution")
    ->required();
  statusCommand->add_flag(
    "-f,--force-refresh", forceRefresh,
    "Force a refresh by requesting the status from the responsible process "
    "engine."
    "If not set, the status will only be refreshed for non-final states like "
    "RUNNING or NEEDS_INTERACTION.");

  CLI::App *startCommand =
    app.add_subcommand("start", "Start a workflow from a .flow file");
  startCommand
    ->add_option("file", workflowFile, "Workflow description file (.flow)")
    ->required();
  startCommand->add_option(
    "-e,--engine", workflowEngine,
    "Workflow engine for processing. Use --list-engines for a list of "
    "available engines");
  startCommand->add_flag("-C,--no-color", noColors, "Disable terminal colors");

  CLI::App *continueCommand = app.add_subcommand(
    "continue", "Continue execution of a workflow with given id");

  CLI::App *cancelCommand = app.add_subcommand(
    "cancel", "Cancel execution of a workflow with given id");

  CLI::App *logCommand =
    app.add_subcommand("log", "Get the log of given workflow execution");
  CLI::App *logPathCommand = app.add_subcommand(
    "log_path",
    "Get the path to the log file for the given workflow execution");
  CLI::App *shortcutsCommand = app.add_subcommand(
    "shortcuts", "Get a list of shortcuts for the given workflow execution");
  CLI::App *interactionsCommand = app.add_subcommand(
    "interactions",
    "Get interaction descriptions for given workflow execution");

  CLI::App *inputInteractionValueCommand =
    app.add_subcommand("input", "Provide input value for requested user "
                                "interaction to the workflow engine");
  inputInteractionValueCommand
    ->add_option("workflowId", workflowId, "Id of the workflow execution")
    ->required();
  inputInteractionValueCommand
    ->add_option("interactionId", interactionId, "Id of the interaction")
    ->required();
  inputInteractionValueCommand->add_option("value", inputValue, "Value")
    ->required();

  CLI::App *serveCommand = app.add_subcommand("serve", "Serve REST-Requests");
  serveCommand->add_option(
    "-P,--port", servePort,
    "Port for the REST service (default: " + std::to_string(servePort) + ")");
  serveCommand->add_option(
    "-e,--endpoint", serveEndpoint,
    "Endpoint to serve at, for example /api/, defaults to " + serveEndpoint);
  serveCommand->add_option("-H,--host", serveHost,
                           "Server host, defaults to " + serveHost);

  auto serveX509PathOption = serveCommand->add_option(
    "-x,--ssl-x509", serveX509Path, "Path to the X509 certificate.");

  auto servePrivateKeyPathOption =
    serveCommand->add_option("-k,--ssl-key", servePrivateKeyPath,
                             "Path to the private key of the SSL sertificate.");

  serveCommand->add_flag("-s,--ssl", serveUseSsl, "Use SSL.")
    ->needs(serveX509PathOption)
    ->needs(servePrivateKeyPathOption);


  // add id argument to all commands which need the workflow id
  for (auto command : {continueCommand, logCommand, logPathCommand,
                       shortcutsCommand, interactionsCommand, cancelCommand}) {
    command->add_option("id", workflowId, "Id of the workflow execution")
      ->required();
  }

  CLI11_PARSE(app, argc, argv);

  if (path.back() != '/') { path += '/'; }

  ProcessManager manager(path, "workflows/", "engines.json", "status.json");

  try {
    if (*listWorkflowsCommand) {
      std::cout << std::setw(4) << manager.getWorkflowList() << std::endl;
    } else if (*listEnginesCommand) {
      std::cout << std::setw(4) << manager.getEnginesList() << std::endl;
    } else if (*statusCommand) {
      std::cout << std::setw(4)
                << manager.getWorkflowStatus(workflowId, forceRefresh)
                << std::endl;
    } else if (*startCommand) {
      manager.startWorkflow(workflowFile, workflowEngine, noColors);
    } else if (*logCommand) {
      std::cout << manager.getLog(workflowId) << std::endl;
    } else if (*logPathCommand) {
      std::cout << manager.getLogPath(workflowId) << std::endl;
    } else if (*shortcutsCommand) {
      std::cout << manager.getShortcuts(workflowId) << std::endl;
    } else if (*interactionsCommand) {
      std::cout << manager.getInteractions(workflowId) << std::endl;
    } else if (*continueCommand) {
      manager.continueWorkflow(workflowId);
    } else if (*cancelCommand) {
      manager.cancelWorkflow(workflowId);
    } else if (*inputInteractionValueCommand) {
      manager.inputInteractionValue(workflowId, interactionId, inputValue);
    } else if (*serveCommand) {
      net::RestService::Config serviceConfig{.endpoint = serveEndpoint,
                                             .host = serveHost,
                                             .port = servePort,
                                             .useSsl = serveUseSsl,
                                             .x509Path = serveX509Path,
                                             .privateKeyPath =
                                               servePrivateKeyPath};

      webapi::ProcessManagerService service(&manager, serviceConfig);
      service.serve();
    }
  } catch (const std::exception &exception) {
    std::cerr << exception.what() << std::endl;
    manager.log("Error: " + std::string(exception.what()));
    return 1;
  }

  return 0;
}
