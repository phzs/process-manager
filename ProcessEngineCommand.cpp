/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "ProcessEngineCommand.h"
#include "ShellResult.h"
#include "config.h"
#include "process.h"
#include <condition_variable>
#include <iostream>
#include <process.hpp>

using namespace std::chrono_literals;

ProcessEngineCommand::ProcessEngineCommand(std::string path,
                                           const std::string &subcommand)
    : path(std::move(path)) {
  addArgument(subcommand);
}

void ProcessEngineCommand::addArgument(std::string argument) {
  arguments.push_back(std::move(argument));
}

void ProcessEngineCommand::executeNonBlocking() {
  process::runDetached(getFullCommand());
}

ShellResult ProcessEngineCommand::execute(bool ignoreTimeout) {
  std::string parameterizedCommand = getFullCommandString();
  std::string processEngineStdout, processEngineStderr;
  std::condition_variable cv;
  int exit_status = 0;
  std::thread thread([&]() {
    TinyProcessLib::Process runProcess(
      parameterizedCommand, "",
      [&](const char *bytes, size_t n) {
        processEngineStdout.append(std::string(bytes, n));
      },
      [&](const char *bytes, size_t n) {
        processEngineStderr.append(std::string(bytes, n));
      });
    exit_status = runProcess.get_exit_status();
    cv.notify_one();
  });

  if (ignoreTimeout) {
    thread.join();
  } else {
    std::mutex mutex;
    std::unique_lock<std::mutex> lock(mutex);
    if (cv.wait_for(lock,
                    std::chrono::seconds(config::ENGINE_TIMEOUT_SECONDS)) ==
        std::cv_status::timeout) {
      std::cerr << "Warning: Timeout while waiting for the Process Engine. The "
                   "Process Engine "
                   "must return either a status or an error for the execution "
                   "job immediately."
                << std::endl;
      thread
        .detach(); // the destructor of std::thread will call terminate() if the thread is joinable
    } else {
      thread.join();
    }
  }

  return ShellResult{processEngineStdout, processEngineStderr, exit_status};
}

std::vector<std::string> ProcessEngineCommand::getFullCommand() const {
  std::vector<std::string> result;
  result.reserve(arguments.size() + 1);

  // important: add base command here, then arguments
  result.emplace_back(path);

  std::copy(arguments.cbegin(), arguments.cend(), std::back_inserter(result));
  return result;
}

std::string ProcessEngineCommand::getFullCommandString() const {
  std::stringstream stream;
  std::vector<std::string> fullCommand = getFullCommand();
  for (auto iter = fullCommand.begin(); iter != fullCommand.end(); ++iter) {
    stream << *iter;
    if ((iter + 1) != fullCommand.end()) { stream << " "; }
  }
  return stream.str();
}
