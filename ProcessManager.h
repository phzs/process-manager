/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "domain/ProcessEngine.h"
#include "domain/Workflow.h"
#include <memory>
#include <string>
#include <vector>

class ProcessManager {

public:
  ProcessManager(std::string basePath, std::string workflowDirectory,
                 std::string engineFile, std::string statusFile);

  // interface to request information
  json getWorkflowList();
  json getEnginesList() const;
  json getWorkflowStatus(unsigned int workflowId, bool forceRefresh = false);
  std::string getLog(unsigned int workflowId);
  std::string getLogPath(unsigned int workflowId) const;
  json getShortcuts(unsigned int workflowId);
  json getInteractions(unsigned int workflowId);
  bool hasWorkflow(unsigned int workflowId);

  // interface to start / continue workflow execution or insert values
  json startWorkflow(const std::string &workflowFile,
                     const std::string &workflowEngine, bool noColors = false);
  void continueWorkflow(unsigned int workflowId);
  void cancelWorkflow(unsigned int workflowId);
  void inputInteractionValue(unsigned int workflowId, std::string interactionId,
                             const std::string &inputValue);

  const std::string &getBasePath() const;

  // public interface for other classes
  void log(const std::string &message) const;
  void error(const std::string &message) const;

private:
  void loadAvailableEngineConfigurations();

  void restore();
  void save(int nextId) const;
  void save() const;

  const ProcessEngine *getProcessEngine(const std::string &engineName,
                                        bool logErrors = true) const;
  Workflow *getExistingWorkflow(unsigned int workflowId,
                                bool logErrors = true) const;


  json retrieveWorkflowStatus(const std::string &workflowFolder,
                              const std::string &engineName) const;
  json retrieveWorkflowStatus(unsigned int id) const;
  std::string getWorkflowExecutionFolder(unsigned int id) const;

  std::string basePath;
  std::string workflowsDirectory;
  std::string engineFile;
  std::string workflowFile;

  std::string defaultProcessEngine;
  int nextId;
  std::unordered_map<unsigned int, std::unique_ptr<Workflow>> workflows;
  std::unordered_map<std::string, std::unique_ptr<ProcessEngine>>
    processEngines;

  bool updateWorkflow(Workflow *workflow, bool forceRefresh = false) const;
};
