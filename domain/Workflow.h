/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "Serializable.h"
#include <optional>
#include <string>

enum WorkflowState {
  READY,
  RUNNING,
  NEEDS_INTERACTION,
  CANCELLING,
  CANCELLED,
  ERROR,
  FINISHED
};

class Workflow : public Serializable {

public:
  Workflow() = default;
  Workflow(int id, std::string fileName, WorkflowState state,
           std::string startDateTime, std::string endDateTime,
           std::string processEngine, int nodesProcessed, int nodesTotal);

  std::string getFileName() const;
  WorkflowState getState() const;
  void setState(WorkflowState state);
  const std::string &getStartDateTime() const;
  const std::string &getEndDateTime() const;
  std::string getProcessEngine() const;
  unsigned int getId() const;
  void setId(unsigned int id);
  std::optional<int> getPid() const;

  static std::string stateToString(WorkflowState state);
  static WorkflowState stringToState(const std::string &stringState);

  json getJson() const override;
  void fromJson(json object) override;

private:
  unsigned int id;
  std::string fileName;
  WorkflowState state;
  std::string startDateTime;
  std::string endDateTime;
  std::string processEngine;
  std::optional<int> pid;
  int nodesProcessed;
  int nodesTotal;
};
