/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */


#include "Workflow.h"

#include <iomanip>
#include <iostream>
#include <utility>


Workflow::Workflow(int id, std::string fileName, WorkflowState state,
                   std::string startDateTime, std::string endDateTime,
                   std::string processEngine, int nodesProcessed,
                   int nodesTotal)
    : id(id), fileName(std::move(fileName)), state(state),
      startDateTime(std::move(startDateTime)),
      endDateTime(std::move(endDateTime)),
      processEngine(std::move(processEngine)), nodesProcessed(nodesProcessed),
      nodesTotal(nodesTotal) {}

std::string Workflow::getFileName() const { return fileName; }

WorkflowState Workflow::getState() const { return state; }
void Workflow::setState(WorkflowState state) { Workflow::state = state; }

const std::string &Workflow::getStartDateTime() const { return startDateTime; }

const std::string &Workflow::getEndDateTime() const { return endDateTime; }

std::string Workflow::getProcessEngine() const { return processEngine; }

std::string Workflow::stateToString(WorkflowState state) {
  switch (state) {
    case READY:
      return "Ready";
    case RUNNING:
      return "Running";
    case NEEDS_INTERACTION:
      return "Needs_interaction";
    case CANCELLING:
      return "Cancelling";
    case CANCELLED:
      return "Cancelled";
    case ERROR:
      return "Error";
    case FINISHED:
      return "Finished";
    default:
      return "Unknown state";
  }
}

WorkflowState Workflow::stringToState(const std::string &stringState) {
  if (stringState == "Ready") {
    return READY;
  } else if (stringState == "Running") {
    return RUNNING;
  } else if (stringState == "Needs_interaction") {
    return NEEDS_INTERACTION;
  } else if (stringState == "Cancelling") {
    return CANCELLING;
  } else if (stringState == "Cancelled") {
    return CANCELLED;
  } else if (stringState == "Error") {
    return ERROR;
  } else if (stringState == "Finished") {
    return FINISHED;
  } else {
    throw std::logic_error("Unknown state string: " + stringState);
  }
}

json Workflow::getJson() const {
  json jsonObj;
  jsonObj["id"] = id;
  jsonObj["fileName"] = fileName;
  jsonObj["state"] = stateToString(state);
  jsonObj["startDateTime"] = startDateTime;
  jsonObj["endDateTime"] = endDateTime;
  jsonObj["processEngine"] = processEngine;
  jsonObj["nodesProcessed"] = nodesProcessed;
  jsonObj["nodesTotal"] = nodesTotal;
  if (pid) { jsonObj["pid"] = pid.value(); }
  return jsonObj;
}

void Workflow::fromJson(json object) {
  if (object.empty() || object == nullptr) {
    throw std::invalid_argument("Json object is empty or null.");
  }
  if (object.contains("id")) { id = object["id"].get<int>(); }
  fileName = object["fileName"].get<std::string>();
  state = stringToState(object["state"].get<std::string>());
  startDateTime = object["startDateTime"].get<std::string>();
  endDateTime = object["endDateTime"].get<std::string>();
  if (object.contains("processEngine")) {
    processEngine = object["processEngine"].get<std::string>();
  }
  nodesProcessed = object["nodesProcessed"].get<int>();
  nodesTotal = object["nodesTotal"].get<int>();
  if (object.contains("pid")) { pid = object["pid"].get<int>(); }
}

void Workflow::setId(unsigned int id) { Workflow::id = id; }

unsigned int Workflow::getId() const { return id; }

std::optional<int> Workflow::getPid() const { return pid; }
